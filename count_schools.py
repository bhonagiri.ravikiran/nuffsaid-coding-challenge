import csv
import time

def print_counts():
    school_count_list = []
    schools_by_state_map = {}
    metro_centric_locale_map = {}
    city_school_count_map = {}
    unique_ids = [] ## to check the school count and id's match

    with open('school_data.csv', mode='r', encoding='cp1252') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:

                school_count_list.append(row[3])
                unique_ids.append(row[0])

                if row[4] in city_school_count_map:
                    city_school_count_map[row[4]] += 1
                else:
                    city_school_count_map[row[4]] = 1

                if row[5] in schools_by_state_map:
                    schools_by_state_map[row[5]] += 1
                else:
                    schools_by_state_map[row[5]] = 1

                if row[8] in metro_centric_locale_map:
                    metro_centric_locale_map[row[8]] += 1
                else:
                    metro_centric_locale_map[row[8]] = 1

            line_count += 1

        city_key = max(city_school_count_map, key=city_school_count_map.get)

        print(f"Total Schools: {len((school_count_list))}")
        print(f"Schools by State:")
        for k in schools_by_state_map.keys():
            print(f"{k}: {schools_by_state_map[k]}")
        print(f"Schools by Metro-centric locale:")
        for k in metro_centric_locale_map.keys():
            print(f"{k}: {metro_centric_locale_map[k]}")
        print(f"City with most schools: {city_key} ({city_school_count_map[city_key]} schools)")
        print(f"Unique cities with at least one school: {len(city_school_count_map)}")
        #print(len(unique_ids))
        #print(len(list(set(unique_ids))))


if __name__ == "__main__":

    start = time.time()
    print_counts()
    end = time.time()
    print(end - start)
