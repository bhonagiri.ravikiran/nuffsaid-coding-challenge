import csv
import time
import itertools

state_lookup = {
"AL":"Alabama",
"AK":"Alaska",
"AZ":"Arizona",
"AR":"Arkansas",
"CA":"California",
"CO":"Colorado",
"CT":"Connecticut",
"DE":"Delaware",
"DC":"District of Columbia",
"FL":"Florida",
"GA":"Georgia",
"HI":"Hawaii",
"ID":"Idaho",
"IL":"Illinois",
"IN":"Indiana",
"IA":"Iowa",
"OR":"Oregon",
"NV":"Nevada",
"MD":"MaryLand",
"OH":"Ohio",
"C":"nostate",
}

def preprocess_string(m_str):
    ignorelist = ["school", "high", "middle", "elementary", "primary", "junior"]
    m_str = m_str.strip().lower()
    llist = [ele for ele in m_str.split() if ele not in ignorelist]
    return llist

def rank_results(llist, input_query):
    count_list = []
    for tup in llist:
        count = 0
        for word in input_query.split():
            if word in tup[0].strip().lower().split():
                count += 1
        count_list.append(count)

    return list(reversed(sorted(range(len(count_list)), key = lambda sub: count_list[sub])[-3:]))

### main function
### removing ignored words
### making keywords and searching in the corpos
def search_schools(input_query):
    input_keyword_list = preprocess_string(input_query)
    search_corpos = {}
    search_results_list = []
    top_answer_list = []
    with open('school_data.csv', mode='r', encoding='cp1252') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                str = row[3] + " " + row[4] + " " + state_lookup[row[5].strip()]
                search_corpos[str.lower().strip()] = row[3] + " " + row[4] + ", " + row[5]
                line_count += 1


    start = time.time()
    ## getting the search results list
    search_results_list = [(my_str, search_corpos[my_str]) for my_str in search_corpos.keys() if all((word in my_str) for word in input_keyword_list)]
    ## ranking function
    top_answer_list = rank_results(search_results_list, input_query)
    end = time.time()


    if len(top_answer_list) != 0:
        print(f"Results for \"{input_query}\" (search took: {round(end-start,3)}s)")
        count = 1
        for index in top_answer_list:
            print(f"{count}. {search_results_list[index][1]}")
            count += 1
    else:
        print(f"No search results found -- please use proper keywords")



if __name__ == "__main__":

    print(f"\n")

    search_schools("elementary school highland park")

    print(f"\n")

    search_schools("jefferson belleville")

    print(f"\n")

    search_schools("riverside school 44")

    print(f"\n")

    search_schools("foley high alabama")

    print(f"\n")

    search_schools("KUSKOKWIM")

    print(f"\n")
